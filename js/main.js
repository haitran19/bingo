//Jquery
$(document).ready(function() {
	//sự kiện click nút Start game tạo phòng
	$('#create').click(function(event) {
		//lấy giá trị của từng ô inputt
		var people = document.getElementById('n').value;
		var num = 25;
		var val = document.getElementById('nnn').value;
		//validate
		if($('#nnn').val() >= num && !isNaN(people) && !isNaN(val))
		{
			$('.Box').addClass('hide');
			$('.content').addClass('display');
			$('.setup').addClass('display');
			document.getElementById('txt1').value = people;
			document.getElementById('txt3').value = val;
		}
	});
	//sự kiện click nút menu
	$('.menu').click(function(event) {
		$('.content').addClass('menus-fix');
		$('.menus').addClass('menu-fix');
	});
	//sự kiện click nút cancel
	$('#cancel').click(function(event) {
		$('.content').removeClass('menus-fix');
		$('.menus').removeClass('menu-fix');
	});

});

//JavaScript
document.addEventListener("DOMContentLoaded", function(){

	//tạo hàm có tên là createPeople
	var createPeople = function(){
		var people = document.getElementById('n').value;
		var num = 25;
		var val = document.getElementById('nnn').value;
		//validate
		if (isNaN(people) || isNaN(val) ){
	   	 	alert("Bạn đang nhập chữ, vui lòng nhập số");
	   	 	return false;
	  	}
	  	//chuyển từ string về number
		people = Number(people);
		val = Number(val);
		//validate
		if(val < num){
			alert("Giá trị thẻ bài phải lớn hơn hoặc bằng 25!");
			return false;
		}
		//Tạo số người chơi
		for(var i=0; i<people; i++){
			$('.append').append('<div class="col-md-4"><div class="bingo bin'+i+'">'+'\
				</div>\
				</div>');
		}
		//Tạo số thẻ bài
		for(var k=0; k<num; k++){
			$('.bingo').append('<span><p>?</p></span>');
		}

		//gọi hàm randumNumber
		randomNumber();
	}

	//tạo hàm có tên createPeople1
	var createPeople1 = function(){

		//lấy giá trị các ô input
		var people = document.getElementById('txt1').value;
		var num = 25;
		var val = document.getElementById('txt3').value;
		//validate
		if (isNaN(people) || isNaN(val) ){
	   	 	alert("Bạn đang nhập chữ, vui lòng nhập số");
	   	 	return false;
	  	}
	  	//chuyển kiểu string về number
		people = Number(people);
		val = Number(val);
		//validate
		if(val < num){
			alert("Giá trị thẻ bài phải lớn hơn hoặc bằng 25!");
			return false;
		}
		//reset lại nội dung div class append
		$('.append').html('');
		//tạo số người chơi
		for(var i=0; i<people; i++){
			$('.append').append('<div class="col-md-4"><div class="bingo bin'+i+'">'+'\
				</div>\
				</div>');
		}
		//tạo số thẻ
		for(var k=0; k<num; k++){
			$('.bingo').append('<span><p>?</p></span>');
		}
		//gọi hàm randomnumber1
		randomNumber1();
	}

	//tạo hàm randomNumber
	var randomNumber = function(){
		//lấy giá trị từ các ô input
		var people = document.getElementById('n').value;
		var num = 25;
		var val = document.getElementById('nnn').value;
		//validate
		if (isNaN(people) || isNaN(val) ){
	   	 	alert("Bạn đang nhập chữ, vui lòng nhập số");
	   	 	return false;
	  	}
	  	//chuyển string về number
		people = Number(people);
		val = Number(val);
		//validate
		if(val < num){
			alert("Giá trị thẻ bài phải lớn hơn hoặc bằng 25!");
			return false;
		}
		//vòng lặp để thêm phần tử mảng
		for(var i=0; i<people; i++){
			//khai báo 1 mảng tên là numbers
			var numbers = [];
			//vòng lặp tạo ra giá trị thẻ bài ứng với số lượng thẻ đã có
			for(var x=0; x<num; x++){

				var randomNumber = Math.floor(Math.random()*val +1);

				if(numbers.indexOf(randomNumber)==-1){
					numbers.push(randomNumber);
				}
				else{
					x--;
				}
			}
			//gán giá trị cho từng thẻ
			var many = document.querySelectorAll('.bin'+i+' span p');
			for(var y=0; y<num; y++){
				if(y==12){
					many[y].innerHTML = "?";
				}
				else{
					many[y].innerHTML = numbers[y];
				}
				
			}
			//xóa mảng
			numbers.splice(0,num);
		}
	}

	//tạo hàm randomNumber1
	var randomNumber1 = function(){
		//lấy giá trị từ các input
		var people = document.getElementById('txt1').value;
		var num = 25;
		var val = document.getElementById('txt3').value;
		//validate
		if (isNaN(people) || isNaN(val) ){
	   	 	alert("Bạn đang nhập chữ, vui lòng nhập số");
	   	 	return false;
	  	}
	  	//chuyển từ string về number
		people = Number(people);
		val = Number(val);
		//validate
		if(val < num){
			alert("Giá trị thẻ bài phải lớn hơn hoặc bằng 25!");
			return false;
		}
		//Thêm giá trị vào mảng
		for(var i=0; i<people; i++){
			var numbers = [];

			for(var x=0; x<num; x++){

				var randomNumber = Math.floor(Math.random()*val +1);

				if(numbers.indexOf(randomNumber)==-1){
					numbers.push(randomNumber);
				}
				else{
					x--;
				}
			}

			//gán giá trị cho từng thẻ
			var many = document.querySelectorAll('.bin'+i+' span p');
			for(var y=0; y<num; y++){
				if(y==12){
					many[y].innerHTML = "?";
				}
				else{
					many[y].innerHTML = numbers[y];
				}
			}
			//xóa mảng
			numbers.splice(0,num);
		}
	}

	//tạo hàm randomNumber2
	var numberss = [];
	var randomNumber2 = function(){
		var val = document.getElementById('txt3').value;
	  	//chuyển từ string về number
		val = Number(val);
		if(numberss.length == val){
			return false;
		}

		var randomNumber = Math.floor(Math.random()*val +1);

		if(numberss.indexOf(randomNumber)==-1){
			numberss.push(randomNumber);
			document.getElementById('p1').innerHTML = randomNumber;
			document.getElementById('p2').innerHTML = randomNumber;
			var P = document.querySelectorAll('.bingo span p');
			for(var j=0; j<P.length; j++){
				if(P[j].innerHTML == randomNumber){
					P[j].style.color = "red";
				}
			}
		}
		else{
			j--;
		}
	}


	//khai báo biến time
	var time;

	//bắt sự kiện phím Enter
	$('#txt1').keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			clearInterval(time);
			numberss = [];
			var P = document.querySelectorAll('.bingo span p');
			for(var j=0; j<P.length; j++){
					P[j].style.color = "#fff";
			}
			document.getElementById('p1').innerHTML = "?";
			document.getElementById('p2').innerHTML = "?";
			createPeople1();
		}

	});

	//bắt sự kiện phím Enter
	$('#txt3').keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			clearInterval(time);
			numberss = [];
			var P = document.querySelectorAll('.bingo span p');
			for(var j=0; j<P.length; j++){
					P[j].style.color = "#fff";
			}
			document.getElementById('p1').innerHTML = "?";
			document.getElementById('p2').innerHTML = "?";
			createPeople1(); 
		}

	});


	//bắt sự kiện nút play
	var play = document.getElementById('play');
	play.addEventListener('click',function(){
		time = setInterval(function(){
			randomNumber2();
		},3000);
	});

	//bắt sự kiện nút reload
	var restart = document.getElementById('reload');
	restart.addEventListener('click',function(){
		clearInterval(time);
		numberss = [];
		var P = document.querySelectorAll('.bingo span p');
		for(var j=0; j<P.length; j++){
				P[j].style.color = "#fff";
		}
		document.getElementById('p1').innerHTML = "?";
		document.getElementById('p2').innerHTML = "?";
		randomNumber1();
	});
	//bắt sự kiện nút tạo mới
	var news = document.getElementById('new');
	news.addEventListener('click',function(){
		clearInterval(time);
		numberss = [];
		var P = document.querySelectorAll('.bingo span p');
		for(var j=0; j<P.length; j++){
				P[j].style.color = "#fff";
		}
		document.getElementById('p1').innerHTML = "?";
		document.getElementById('p2').innerHTML = "?";
		createPeople1();
	});
	//bắt sự kiện nút off
	var off = document.getElementById('off');
	off.addEventListener('click',function(){
		location.reload();
	});
	//bắt sự kiện nút tạo phòng start game
	var create = document.getElementById('create');
	create.addEventListener('click',function(){
		createPeople();
	});
});